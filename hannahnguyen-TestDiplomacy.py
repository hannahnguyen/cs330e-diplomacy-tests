#!/usr/bin/env python3

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, armies, locations, actions, loc_dict, armies_dict

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    # def test_read_1(self):
    #     s = "A Madrid Hold"
    #     diplomacy_read(s)
    #     self.assertEqual(armies, ["A"])


    # def test_read_2(self):
    #     s = "B Barcelona Move Madrid"
    #     diplomacy_read(s)
    #     self.assertEqual(locations, ["Madrid", "Barcelona"])

    # def test_read_3(self):
    #     s = "C London Support A"
    #     diplomacy_read(s)
    #     self.assertEqual(actions, [("Hold", ""), ("Move", "Madrid"), ("Support", "A")])

    #     armies.clear()
    #     locations.clear()
    #     actions.clear()
    #     loc_dict.clear()
    #     armies_dict.clear()


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\nD Tokyo Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\nD London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\nD Tokyo Move London\nE Delhi Support A\nF Mumbai Support B\nG Beijing Move Delhi")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC Barcelona\nD London\nE [dead]\nF Mumbai\nG [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\nD Tokyo Move London\nE Delhi Support A\nF Mumbai Support B\nG Beijing Move Delhi\nH Brazil Support E")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC Barcelona\nD London\nE Delhi\nF Mumbai\nG [dead]\nH Brazil\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()
